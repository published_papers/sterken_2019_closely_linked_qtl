            
    variance.test <- function(trait.matrix,variables,method_padjust){
                              if(missing(trait.matrix)){       stop("traits missing")}
                              if(missing(variables)){          stop("variables missing")}
                              if(missing(method_padjust)){     method_padjust <- "fdr"}
                              
                              ###vartest function
                              conv.vartest <- function(y,x){return(c(fligner.test(y,x)$p.value,var(y[x==1],na.rm=T),var(y[x==-1],na.rm=T)))}
                              #conv.vartest <- function(y,x){return(c(var.test(y[x==1],y[x==-1],ratio=1,alternative="two.sided")$p.value,var(y[x==1]),var(y[x==-1])))}
                              
                              #perm.vartest <- function(y,x){return(bartlett.test(y,x)$p.value)}
                              
                              output <- t(apply(trait.matrix,1,conv.vartest,x=variables))
                              output <- cbind(output,p.adjust(output[,1],method=method_padjust))
    }        
